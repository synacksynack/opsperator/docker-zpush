FROM opsperator/php

# Z-Push image for OpenShift Origin

LABEL io.k8s.description="Z-Push Server." \
      io.k8s.display-name="Z-Push" \
      io.openshift.expose-services="8080:http" \
      io.openshift.tags="z-push" \
      io.openshift.non-scalable="false" \
      help="For more information visit https://gitlab.com/synacksynack/opsperator/docker-zpush" \
      maintainer="Samuel MARTIN MORO <faust64@gmail.com>" \
      version="2.6.4"

USER root
COPY config/* /

RUN set -ex \
    && rm -rf /var/lib/apt/lists/* \
    && apt-get -y update \
    && if test "$DO_UPGRADE"; then \
	echo "# Upgrade Base Image"; \
	apt-get -y upgrade; \
	apt-get -y dist-upgrade; \
    fi \
    && echo "# Install Z-Push Dependencies" \
    && apt-get -y install ldap-utils openssl postgresql-client mariadb-client \
    && savedAptMark="$(apt-mark showmanual)" \
    && apt-get -y install libicu-dev libldap2-dev gnupg2 libpq-dev \
	apt-transport-https libfreetype6-dev libxml2-dev libonig-dev \
	libcurl4-openssl-dev libmemcached-dev libc-client-dev libkrb5-dev \
	apt-transport-https \
    && docker-php-ext-configure imap --with-kerberos --with-imap-ssl \
    && docker-php-ext-configure ldap --with-libdir="lib/$debMultiarch" \
    && docker-php-ext-install bcmath exif intl ldap opcache pcntl pdo_mysql \
	pdo_pgsql imap soap xml \
    && docker-php-ext-install mbstring intl ldap \
    && pecl install APCu-5.1.21 \
    && pecl install memcached-3.2.0 \
    && pecl install redis-5.3.7 \
    && docker-php-ext-enable apcu redis imap soap xml \
    && apt-mark auto '.*' >/dev/null \
    && apt-mark manual $savedAptMark \
    && ldd "$(php -r 'echo ini_get("extension_dir");')"/*.so \
	| awk '/=>/ { print $3 }' | sort -u | xargs -r dpkg-query -S \
	| cut -d: -f1 | sort -u | xargs -rt apt-mark manual \
    && echo "# Install Z-Push" \
    && curl -fsSL -o - \
	https://download.kopano.io/zhub/z-push:/final/Debian_10/Release.key \
	| apt-key add - \
    && mv /z-push.list /etc/apt/sources.list.d/ \
    && if test `uname -m` = aarch64; then \
	sed 's|amd64|arm64|g' /dpkg-patch >>/var/lib/dpkg/status \
	&& for dep in php-common php-mysql php-ldap php imap cli mbstring \
		soap; do \
	    echo "/." >/var/lib/dpkg/info/$dep:arm64.list; \
	done; \
    elif test `uname -m` = armv7l; then \
	sed 's|amd64|armhf|g' /dpkg-patch >>/var/lib/dpkg/status \
	&& for dep in php-common php-mysql php-ldap php imap cli mbstring \
		soap; do \
	    echo "/." >/var/lib/dpkg/info/$dep:armhf.list; \
	done; \
    else \
	cat /dpkg-patch >>/var/lib/dpkg/status \
	&& for dep in php-common php-mysql php-ldap php imap cli mbstring \
		soap; do \
	    echo "/." >/var/lib/dpkg/info/$dep:amd64.list; \
	done; \
    fi \
    && apt-get -y update \
    && apt-get -y install z-push-common z-push-state-sql z-push-backend-imap \
	z-push-backend-galsearch-ldap z-push-backend-ldap z-push-autodiscover \
	z-push-ipc-memcached \
    && mv /etc/z-push /etc/z-push.orig \
    && echo "# Fixing permissions" \
    && for dir in /var/log/z-push /var/lib/z-push /etc/z-push \
	    /usr/share/z-push; \
	do \
	    mkdir -p $dir 2>/dev/null \
	    && chown -R 1001:root $dir \
	    && chmod -R g=u $dir; \
	done \
    && echo "# Cleaning up" \
    && apt-get remove --purge -y gnupg2 \
    && apt-get purge -y --auto-remove -o \
	APT::AutoRemove::RecommendsImportant=false \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/* /usr/share/doc /usr/share/man \
	/usr/src/php.tar.xz /dpkg-patch \
    && unset HTTP_PROXY HTTPS_PROXY NO_PROXY DO_UPGRADE http_proxy https_proxy

CMD "/usr/sbin/apache2ctl" "-D" "FOREGROUND"
ENTRYPOINT ["dumb-init","--","/run-zpush.sh"]
USER 1001
