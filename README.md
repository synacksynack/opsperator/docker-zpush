# k8s Z-Push

Z-Push image.

Diverts from https://gitlab.com/synacksynack/opsperator/docker-php

Depends on a Cyrus server, such as
https://gitlab.com/synacksynack/opsperator/docker-cyrus

Depends on an OpenLDAP server, such as
https://gitlab.com/synacksynack/opsperator/docker-openldap

Depends on a Postfix server, such as
https://gitlab.com/synacksynack/opsperator/docker-postfix

Build with:

```
$ make build
```

Test with:

```
$ make run
```

Start Demo in OpenShift:

```
$ make ocdemo
```

Cleanup OpenShift assets:

```
$ make ocpurge
```

Environment variables and volumes
----------------------------------

The image recognizes the following environment variables that you can set during
initialization by passing `-e VAR=VALUE` to the Docker `run` command.

|    Variable name           |    Description              | Default                                                     | Inherited From    |
| :------------------------- | --------------------------- | ----------------------------------------------------------- | ----------------- |
|  `APACHE_DOMAIN`           | Z-Push ServerName           | `push.demo.local`                                           | opsperator/apache |
|  `APACHE_HTTP_PORT`        | Z-Push HTTP(s) Port         | `8080`                                                      | opsperator/apache |
|  `IMAP_HOST`               | Z-Push IMAP Host            | `cyrus.demo.local`                                          |                   |
|  `IMAP_PORT`               | Z-Push IMAP Port            | `143`                                                       |                   |
|  `IMAP_PROTO`              | Z-Push IMAP Protocol        | `imap`                                                      |                   |
|  `IMAP_VENDOR`             | Z-Push IMAP Vendor          | `none`                                                      |                   |
|  `MEMCACHED_HOST`          | Memcached Host              | undef                                                       |                   |
|  `MEMCACHED_PORT`          | Memcached Port              | `11211`                                                     |                   |
|  `MYSQL_DATABASE`          | MySQL Database Name         | `zpush`                                                     |                   |
|  `MYSQL_HOST`              | MySQL Database Host         | `127.0.0.1`                                                 |                   |
|  `MYSQL_PASSWORD`          | MySQL Database Password     | undef                                                       |                   |
|  `MYSQL_PORT`              | MySQL Database Port         | `3306`                                                      |                   |
|  `MYSQL_USER`              | MySQL Database Username     | `zpush`                                                     |                   |
|  `ONLY_TRUST_KUBE_CA`      | Don't trust base image CAs  | `false`, any other value disables ca-certificates CAs       | opsperator/apache |
|  `OPENLDAP_BASE`           | OpenLDAP Base               | seds `OPENLDAP_DOMAIN`, default produces `dc=demo,dc=local` | opsperator/apache |
|  `OPENLDAP_BIND_DN_RREFIX` | OpenLDAP Bind DN Prefix     | `cn=zpush,ou=services`                                      | opsperator/apache |
|  `OPENLDAP_BIND_PW`        | OpenLDAP Bind Password      | `secret`                                                    | opsperator/apache |
|  `OPENLDAP_DOMAIN`         | OpenLDAP Domain Name        | `demo.local`                                                | opsperator/apache |
|  `OPENLDAP_HOST`           | OpenLDAP Backend Address    | `127.0.0.1`                                                 | opsperator/apache |
|  `OPENLDAP_PORT`           | OpenLDAP Bind Port          | `389` or `636` depending on `OPENLDAP_PROTO`                | opsperator/apache |
|  `OPENLDAP_PROTO`          | OpenLDAP Proto              | `ldap`                                                      | opsperator/apache |
|  `PHP_ERRORS_LOG`          | PHP Errors Logs Output      | `/proc/self/fd/2`                                           | opsperator/php    |
|  `PHP_MAX_EXECUTION_TIME`  | PHP Max Execution Time      | `30` seconds                                                | opsperator/php    |
|  `PHP_MAX_FILE_UPLOADS`    | PHP Max File Uploads        | `20`                                                        | opsperator/php    |
|  `PHP_MAX_POST_SIZE`       | PHP Max Post Size           | `8M`                                                        | opsperator/php    |
|  `PHP_MAX_UPLOAD_FILESIZE` | PHP Max Upload File Size    | `2M`                                                        | opsperator/php    |
|  `PHP_MEMORY_LIMIT`        | PHP Memory Limit            | `-1` (no limitation)                                        | opsperator/php    |
|  `POSTGRES_DB`             | Postgres Database Name      | `zpush`                                                     |                   |
|  `POSTGRES_HOST`           | Postgres Database Host      | `127.0.0.1`                                                 |                   |
|  `POSTGRES_PASSWORD`       | Postgres Database Password  | undef                                                       |                   |
|  `POSTGRES_PORT`           | Postgres Database Port      | `5432`                                                      |                   |
|  `POSTGRES_USER`           | Postgres Database Username  | `zpush`                                                     |                   |
|  `PUBLIC_PROTO`            | Apache Public Proto         | `http`                                                      | opsperator/apache |
|  `SMTP_HELO`               | Z-Push SMTP HELO FQDN       | `$APACHE_DOMAIN`                                            |                   |
|  `SMTP_HOST`               | Z-Push SMTP Relay           | `smtp.demo.local`                                           |                   |
|  `SMTP_PORT`               | Z-Push SMTP Port            | `25`                                                        |                   |
|  `SMTP_PROTO`              | Z-Push SMTP Protocol        | `smtp`                                                      |                   |


You can also set the following mount points by passing the `-v /host:/container`
flag to Docker.

|  Volume mount point | Description                     | Inherited From    |
| :------------------ | ------------------------------- | ----------------- |
|  `/certs`           | Apache Certificate (optional)   | opsperator/apache |
