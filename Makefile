SKIP_SQUASH?=1
IMAGE=opsperator/zpush
-include Makefile.cust

.PHONY: build
build:
	@@SKIP_SQUASH=$(SKIP_SQUASH) hack/build.sh

.PHONY: kubebuild
kubebuild: kubecheck
	@@for f in image git task pipeline pipelinerun; \
	    do \
		kubectl apply -f deploy/kubernetes/tekton-$$f.yaml; \
	    done

.PHONY: kubecheck
kubecheck:
	@@kubectl version >/dev/null 2>&1 || exit 42

.PHONY: kubedeploy
kubedeploy: kubecheck
	@@for f in secret service config rbac statefulset deployment; \
	    do \
		kubectl apply -f deploy/kubernetes/$$f.yaml; \
	    done

.PHONY: ocbuild
ocbuild: occheck
	@@oc process -f deploy/openshift/imagestream.yaml | oc apply -f-
	@@BRANCH=`git rev-parse --abbrev-ref HEAD`; \
	if test "$$GIT_DEPLOYMENT_TOKEN"; then \
	    oc process -f deploy/openshift/build-with-secret.yaml \
		-p "GIT_DEPLOYMENT_TOKEN=$$GIT_DEPLOYMENT_TOKEN" \
		-p "CYRUS_REPOSITORY_REF=$$BRANCH" \
		| oc apply -f-; \
	else \
	    oc process -f deploy/openshift/build.yaml \
		-p "CYRUS_REPOSITORY_REF=$$BRANCH" \
		| oc apply -f-; \
	fi

.PHONY: occheck
occheck:
	@@oc whoami >/dev/null 2>&1 || exit 42

.PHONY: ocpurge
ocpurge: occheck
	@@oc process -f deploy/openshift/build.yaml | oc delete -f- || true
	@@oc process -f deploy/openshift/imagestream.yaml \
	    | oc delete -f- || true

.PHONY: testldap
testldap:
	@@docker rm -f testldap || echo ok
	@@docker run --name testldap \
	    --tmpfs /var:rw,size=65536k \
	    --tmpfs /var/run:rw,size=65536k  \
	    --tmpfs /run/ldap:rw,size=65536k  \
	    --tmpfs /usr/local/openldap/etc/openldap:rw,size=65536k \
	    --tmpfs /var/lib/ldap:rw,size=65536k \
	    -e OPENLDAP_CYRUS_PASSWORD=secret4242424242 \
	    -e OPENLDAP_DEBUG=yesplease \
	    -e OPENLDAP_GLOBAL_ADMIN_PASSWORD=secret \
	    -e OPENLDAP_POSTFIX_PASSWORD=secret4242424242 \
	    -e OPENLDAP_ZPUSH_PASSWORD=secret4242424242 \
	    -e DEBUG=yesplease \
	    -e DO_CYRUS=true \
	    -e DO_EXPORTERS=true \
	    -e DO_LEMON=true \
	    -e DO_NEXTCLOUD=true \
	    -e DO_POSTFIX=true \
	    -e DO_SSP=true \
	    -e DO_SERVICEDESK=true \
	    -e DO_WHITEPAGES=true \
	    -e DO_ZPUSH=true \
	    -e OPENLDAP_BIND_LDAP_PORT=1389 \
	    -e OPENLDAP_BIND_LDAPS_PORT=1636 \
	    -e OPENLDAP_INIT_DEBUG_LEVEL=256 \
	    -p 1389:1389 -p 1636:1636 \
	    -d opsperator/openldap

.PHONY: testcyrus
testcyrus:
	@@mkdir -p volume/saslauthd volume/rsyslog volume/imap || echo meh
	@@chmod -R 777 volume || echo warning
	@@docker rm -f testcyrusimapd || echo ok
	@@docker rm -f testcyrussyslog || echo ok
	@@docker rm -f testcyrussaslauthd || echo ok
	@@docker rm -f testcyrusjob || echo ok
	@@docker run --name testcyrusimapd \
	    --privileged \
	    -e OPENLDAP_BIND_PW=secret4242424242 \
	    -e OPENLDAP_PROTO=ldap \
	    -e OPENLDAP_PORT=1389 \
	    -v `pwd`/volume/saslauthd:/run/saslauthd \
	    -v `pwd`/volume/rsyslog:/run/rsyslog \
	    -v `pwd`/volume/imap:/var/spool/imap \
	    --tmpfs /var/spool/cyrus:rw,size=65536k  \
	    --tmpfs /var/lib/cyrus:rw,size=65536k  \
	    --tmpfs /var/lib/imap:rw,size=65536k  \
	    -p 143:143 -p 993:993 -p 2400:2400 \
	    -d opsperator/cyrus /start-cyrus.sh
	@@docker run --name testcyrussyslog \
	    --privileged \
	    -v `pwd`/volume/rsyslog:/run/rsyslog \
	    -d opsperator/cyrus /start-syslog.sh
	@@docker run --name testcyrussaslauthd \
	    --privileged \
	    -v `pwd`/volume/saslauthd:/run/saslauthd \
	    -v `pwd`/volume/rsyslog:/run/rsyslog \
	    -d opsperator/cyrus /start-saslauthd.sh
	@@docker run --name testcyrusjob \
	    --privileged \
	    -e OPENLDAP_BIND_PW=secret4242424242 \
	    -e OPENLDAP_PROTO=ldap \
	    -e OPENLDAP_PORT=1389 \
	    -v `pwd`/volume/saslauthd:/run/saslauthd \
	    -v `pwd`/volume/rsyslog:/run/rsyslog \
	    -v `pwd`/volume/imap:/var/spool/imap \
	    -d opsperator/cyrus /run-job.sh

.PHONY: testpostfix
testpostfix:
	@@mkdir -p volume/saslauthd2 volume/rsyslog2 volume/postfix || echo meh
	@@chmod -R 777 volume || echo warning
	@@docker rm -f testpostfixsmtp || echo ok
	@@docker rm -f testpostfixsyslog || echo ok
	@@docker rm -f testpostfixsaslauthd || echo ok
	@@docker rm -f testpostfixjob || echo ok
	@@docker run --name testpostfixsmtp \
	    --privileged \
	    -e DONT_GENERATE_MAPS=yay \
	    -e LMTP_HOST=127.0.0.1 \
	    -e LMTP_PORT=2400 \
	    -v `pwd`/volume/postfix:/etc/postfix \
	    -v `pwd`/volume/saslauthd2:/run/saslauthd \
	    -v `pwd`/volume/rsyslog2:/run/rsyslog \
	    --tmpfs /var/spool/postfix:rw,size=65536k  \
	    -v `pwd`/volume/saslauthd:/var/spool/postfix/var/run/saslauthd \
	    -p 25:25 -p 465:465 -p 587:587 \
	    -d opsperator/postfix /start-postfix.sh
	@@docker run --name testpostfixsyslog \
	    --privileged \
	    -v `pwd`/volume/rsyslog2:/run/rsyslog \
	    -d opsperator/postfix /start-syslog.sh
	@@ldapip=`docker inspect testldap | awk '/"IPAddress": "/{print $$2}' | head -1 | cut -d'"' -f2`; \
	docker run --name testpostfixsaslauthd \
	    --privileged \
	    -e OPENLDAP_BIND_PW=secret4242424242 \
	    -e OPENLDAP_HOST=$$ldapip \
	    -e OPENLDAP_PROTO=ldap \
	    -e OPENLDAP_PORT=1389 \
	    -v `pwd`/volume/saslauthd2:/run/saslauthd \
	    -v `pwd`/volume/rsyslog2:/run/rsyslog \
	    -d opsperator/postfix /start-saslauthd.sh
	@@ldapip=`docker inspect testldap | awk '/"IPAddress": "/{print $$2}' | head -1 | cut -d'"' -f2`; \
	docker run --name testpostfixjob \
	    --privileged \
	    -e LMTP_HOST=127.0.0.1 \
	    -e LMTP_PORT=2400 \
	    -e OPENLDAP_BIND_PW=secret4242424242 \
	    -e OPENLDAP_HOST=$$ldapip \
	    -e OPENLDAP_PROTO=ldap \
	    -e OPENLDAP_PORT=1389 \
	    -v `pwd`/volume/postfix:/etc/postfix \
	    -v `pwd`/volume/saslauthd2:/run/saslauthd \
	    -v `pwd`/volume/rsyslog2:/run/rsyslog \
	    -d opsperator/postfix /run-job.sh

.PHONY: test
test:
	@@docker rm -f testzpush || true
	@@docker rm -f testmysql || true
	@@docker run --name testmysql \
	    -e MYSQL_DATABASE=msdb \
	    -e MYSQL_PASSWORD=mspassword \
	    -e MYSQL_ROOT_PASSWORD=msrootpassword \
	    -e MYSQL_USER=msuser \
	    -p 3306:3306 \
	    -d docker.io/centos/mariadb-102-centos7:latest
	@@sleep 10
	@@ldapip=`docker inspect testldap | awk '/"IPAddress": "/{print $$2}' | head -1 | cut -d'"' -f2`; \
	msip=`docker inspect testmysql | awk '/"IPAddress": "/{print $$2}' | head -1 | cut -d'"' -f2`; \
	imapip=`docker inspect testcyrusimapd | awk '/"IPAddress": "/{print $$2}' | head -1 | cut -d'"' -f2`; \
	smtpip=`docker inspect testpostfixsmtp | awk '/"IPAddress": "/{print $$2}' | head -1 | cut -d'"' -f2`; \
	echo "Working with ldap=$$ldapip mysql=$$msip imap=$$imapip postfix=$$smtpip"; \
	docker run --name testzpush \
	    -e IMAP_HOST=$$imapip \
	    -e IMAP_PORT=143 \
	    -e IMAP_PROTO=imap \
	    -e IMAP_VENDOR=cyrus \
	    -e MYSQL_DATABASE=msdb \
	    -e MYSQL_HOST=$$msip \
	    -e MYSQL_PASSWORD=mspassword \
	    -e MYSQL_USER=msuser \
	    -e OPENLDAP_BIND_PW=secret4242424242 \
	    -e OPENLDAP_HOST=$$ldapip \
	    -e OPENLDAP_PROTO=ldap \
	    -e OPENLDAP_PORT=1389 \
	    -e SMTP_HOST=$$smtpip \
	    -e SMTP_PORT=25 \
	    -e SMTP_PROTO=smtp \
            -d $(IMAGE)
