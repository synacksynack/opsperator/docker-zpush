<?php
define("ANONYMOUS_BIND", false);
define("LDAP_SERVER_URI", "OPENLDAP_PROTO://OPENLDAP_HOST:OPENLDAP_PORT/");
define("LDAP_BIND_USER", "OPENLDAP_BIND_DN_PREFIX,OPENLDAP_BASE");
define("LDAP_BIND_PASSWORD", "OPENLDAP_BIND_PW");
define("LDAP_SEARCH_BASE", "ou=users,OPENLDAP_BASE");
define("LDAP_SEARCH_FILTER", "(|(uid=*SEARCHVALUE*)(cn=*SEARCHVALUE*)(mail=*SEARCHVALUE*))");
define("LDAP_SEARCH_NAME_FALLBACK", true);

global $ldap_field_map;
$ldap_field_map = array(
	SYNC_GAL_DISPLAYNAME    => 'cn',
	SYNC_GAL_PHONE          => 'telephonenumber',
	SYNC_GAL_OFFICE         => '',
	SYNC_GAL_TITLE          => 'title',
	SYNC_GAL_COMPANY        => 'ou',
	SYNC_GAL_ALIAS          => 'uid',
	SYNC_GAL_FIRSTNAME      => 'givenname',
	SYNC_GAL_LASTNAME       => 'sn',
	SYNC_GAL_HOMEPHONE      => 'phone',
	SYNC_GAL_MOBILEPHONE    => 'mobile',
	SYNC_GAL_EMAILADDRESS   => 'mail',
    );
