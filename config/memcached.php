<?php
define('MEMCACHED_SERVERS','MEMCACHED_HOSTS');
define('MEMCACHED_DOWN_LOCK_FILE', '/tmp/z-push-memcache-down');
define('MEMCACHED_DOWN_LOCK_EXPIRATION', 30);
define('MEMCACHED_PREFIX', 'z-push-ipc');
define('MEMCACHED_TIMEOUT', 300);
define('MEMCACHED_MUTEX_TIMEOUT', 5);
define('MEMCACHED_BLOCK_WAIT', 10);
