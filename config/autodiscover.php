<?php
define('AUTODISCOVER_LOGIN_TYPE', AUTODISCOVER_LOGIN_EMAIL);
define('BACKEND_PROVIDER', 'BackendIMAP');
define('BASE_PATH', dirname($_SERVER['SCRIPT_FILENAME']). '/');
define('LOGBACKEND', 'filelog');
define('LOGFILEDIR', '/var/log/z-push/');
define('LOGFILE', LOGFILEDIR . 'autodiscover.log');
define('LOGERRORFILE', LOGFILEDIR . 'autodiscover-error.log');
define('LOGLEVEL', LOGLEVEL_DO_DEBUG);
define('LOGUSERLEVEL', LOGLEVEL);
define('LOG_SYSLOG_HOST', false);
define('SERVERURL', 'PUBLIC_PROTO://APACHE_DOMAIN/Microsoft-Server-ActiveSync');
define('TIMEZONE', 'TZ');
define('USE_FULLEMAIL_FOR_LOGIN', true);
define('ZPUSH_HOST', 'APACHE_DOMAIN');

$specialLogUsers = array();
