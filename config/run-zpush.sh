#!/bin/sh

if test "$DEBUG"; then
    set -x
    DO_DEBUG=DEBUG
else
    DO_DEBUG=INFO
fi
. /usr/local/bin/nsswrapper.sh

APACHE_HTTP_PORT=${APACHE_HTTP_PORT:-8080}
APACHE_DOMAIN=${APACHE_DOMAIN:-push.demo.local}
PUBLIC_PROTO=${PUBLIC_PROTO:-http}
IMAP_HOST=${IMAP_HOST:-cyrus.demo.local}
IMAP_PROTO=${IMAP_PROTO:-imap}
IMAP_VENDOR=${IMAP_VENDOR:-none}
OPENLDAP_BIND_DN_PREFIX="${OPENLDAP_BIND_DN_PREFIX:-cn=zpush,ou=services}"
OPENLDAP_BIND_PW="${OPENLDAP_BIND_PW:-secret}"
OPENLDAP_DOMAIN=${OPENLDAP_DOMAIN:-demo.local}
OPENLDAP_HOST=${OPENLDAP_HOST:-}
OPENLDAP_PROTO=${OPENLDAP_PROTO:-ldap}
SMTP_HOST=${SMTP_HOST:-smtp.demo.local}
SMTP_PROTO=${SMTP_PROTO:-smtp}
TZ=${TZ:-UTC}

if test -z "$IMAP_PORT" -a "$IMAP_PROTO" = imaps; then
    IMAP_PORT=993
elif test -z "$IMAP_PORT"; then
    IMAP_PORT=143
fi
if test "$IMAP_PROTO" = imaps; then
    IMAP_PROTO=ssl://
else
    IMAP_PROTO=
fi
if test "$MEMACHED_HOST"; then
    MEMACHED_PORT=${MEMACHED_PORT:-11211}
    for h in $MEMCACHED_HOST
    do
	if test "$hlist"; then
	    hlist="$hlist,"
	fi
	if echo "$h" | grep : >/dev/null; then
	    hlist="$hlist $h"
	else
	    hlist="$hlist $h:$MEMCACHED_PORT"
	fi
    done
    DO_IPC=IpcMemcachedProvider
else
    DO_IPC=IpcSharedMemoryProvider
    hlist=
fi
if test -z "$OPENLDAP_BASE"; then
    OPENLDAP_BASE=`echo "dc=$OPENLDAP_DOMAIN" | sed 's|\.|,dc=|g'`
fi
if test -z "$OPENLDAP_PORT" -a "$OPENLDAP_PROTO" = ldaps; then
    OPENLDAP_PORT=636
elif test -z "$OPENLDAP_PORT"; then
    OPENLDAP_PORT=389
fi
if test -z "$SMTP_HELO"; then
    SMTP_HELO=$APACHE_DOMAIN
fi
if test -z "$SMTP_PORT" -a "$SMTP_PROTO" = smtps; then
    SMTP_PORT=465
elif test -z "$SMTP_PORT"; then
    SMTP_PORT=25
fi
if test "$SMTP_PROTO" = smtps; then
    SMTP_PROTO=ssl://
else
    SMTP_PROTO=
fi

cpt=0
if test "$POSTGRES_PASSWORD"; then
    POSTGRES_DB=${POSTGRES_DB:-zpush}
    POSTGRES_HOST=${POSTGRES_HOST:-127.0.0.1}
    POSTGRES_PORT=${POSTGRES_PORT:-5432}
    POSTGRES_USER=${POSTGRES_USER:-zpush}
    echo Waiting for Postgres backend ...
    while true
    do
	if echo '\d' | PGPASSWORD="$POSTGRES_PASSWORD" \
		psql -U "$POSTGRES_USER" -h "$POSTGRES_HOST" \
		-p "$POSTGRES_PORT" "$POSTGRES_DB" >/dev/null 2>&1; then
	    echo " Postgres is alive!"
	    break
	elif test "$cpt" -gt 25; then
	    echo "Could not reach Postgres" >&2
	    exit 1
	fi
	sleep 5
	echo Postgres is ... KO
	cpt=`expr $cpt + 1`
    done
    STATEMACHINE=SQL
elif test "$MYSQL_PASSWORD"; then
    MYSQL_DATABASE=${MYSQL_DATABASE:-zpush}
    MYSQL_HOST=${MYSQL_HOST:-127.0.0.1}
    MYSQL_PORT=${MYSQL_PORT:-3306}
    MYSQL_USER=${MYSQL_USER:-zpush}
    echo Waiting for MySQL backend ...
    while true
    do
	if echo SHOW TABLES | mysql -u "$MYSQL_USER" \
		--password="$MYSQL_PASSWORD" -h "$MYSQL_HOST" \
		-P "$MYSQL_PORT" "$MYSQL_DATABASE" >/dev/null 2>&1; then
	    echo " MySQL is alive!"
	    break
	elif test "$cpt" -gt 25; then
	    echo "Could not reach MySQL" >&2
	    exit 1
	fi
	sleep 5
	echo MySQL is ... KO
	cpt=`expr $cpt + 1`
    done
    STATEMACHINE=SQL
else
    STATEMACHINE=FILE
fi
if test "$OPENLDAP_HOST"; then
    echo Waiting for LDAP backend ...
    cpt=0
    while true
    do
	if LDAPTLS_REQCERT=never \
		ldapsearch -H $OPENLDAP_PROTO://$OPENLDAP_HOST:$OPENLDAP_PORT \
		-D "$OPENLDAP_BIND_DN_PREFIX,$OPENLDAP_BASE" \
		-b "ou=users,$OPENLDAP_BASE" \
		-w "$OPENLDAP_BIND_PW" \
		uid >/dev/null 2>&1; then
	    echo " LDAP is alive!"
	    break
	elif test "$cpt" -gt 25; then
	    echo Could not reach OpenLDAP >&2
	    exit 1
	fi
	sleep 5
	echo LDAP is ... KO
	cpt=`expr $cpt + 1`
    done
fi

export APACHE_DOMAIN
export APACHE_HTTP_PORT
export APACHE_IGNORE_OPENLDAP=yay
SSL_INCLUDE=no-ssl
. /usr/local/bin/reset-tls.sh
export RESET_TLS=false

if ! test -s /etc/z-push/z-push.conf.php; then
    echo "Install Z-Push Site Configuration"
    sed -e "s|APACHE_DOMAIN|$APACHE_DOMAIN|" \
	-e "s|DO_DEBUG|$DO_DEBUG|" \
	-e "s|DO_IPC|$DO_IPC|" \
	-e "s|STATEMACHINE|$STATEMACHINE|" \
	-e "s|TZ|$TZ|" \
	/config.php >/etc/z-push/z-push.conf.php
fi

if test "$hlist"; then
    if ! test -s /etc/z-push/memcached.conf.php; then
	echo "Install Z-Push Memcached IPC Configuration"
	sed "s|MEMCACHED_HOSTS|$hlist|" \
	    /memcached.php >/etc/z-push/memcached.conf.php
    fi
fi

if ! test -s /etc/z-push/imap.conf.php; then
    echo "Install Z-Push IMAP Backend Configuration"
    sed -e "s|IMAP_HOST|$IMAP_HOST|" \
	-e "s|IMAP_PROTO|$IMAP_PROTO|" \
	-e "s|IMAP_PORT|$IMAP_PORT|" \
	-e "s|IMAP_VENDOR|$IMAP_VENDOR|" \
	-e "s|OPENLDAP_DOMAIN|$OPENLDAP_DOMAIN|" \
	-e "s|SMTP_HELO|$SMTP_HELO|" \
	-e "s|SMTP_HOST|$SMTP_HOST|" \
	-e "s|SMTP_PROTO|$SMTP_PROTO|" \
	-e "s|SMTP_PORT|$SMTP_PORT|" \
	/imap.php >/etc/z-push/imap.conf.php
fi

if test "$OPENLDAP_HOST"; then
    if ! test -s /etc/z-push/ldap.conf.php; then
	echo "Install Z-Push LDAP Contacts Configuration"
	sed -e "s|OPENLDAP_BASE|$OPENLDAP_BASE|" \
	    -e "s|OPENLDAP_HOST|$OPENLDAP_HOST|" \
	    -e "s|OPENLDAP_PORT|$OPENLDAP_PORT|" \
	    -e "s|OPENLDAP_PROTO|$OPENLDAP_PROTO|" \
	    /ldap.php >/etc/z-push/ldap.conf.php
    fi
    if ! test -s /etc/z-push/galsearch-ldap.php; then
	echo "Install Z-Push LDAP GAL"
	sed -e "s|OPENLDAP_BASE|$OPENLDAP_BASE|" \
	    -e "s|OPENLDAP_BIND_DN_PREFIX|$OPENLDAP_BIND_DN_PREFIX|" \
	    -e "s|OPENLDAP_BIND_PW|$OPENLDAP_BIND_PW|" \
	    -e "s|OPENLDAP_HOST|$OPENLDAP_HOST|" \
	    -e "s|OPENLDAP_PORT|$OPENLDAP_PORT|" \
	    -e "s|OPENLDAP_PROTO|$OPENLDAP_PROTO|" \
	    /galsearch-ldap.php >/etc/z-push/galsearch-ldap.conf.php
    fi
fi

if test "$STATEMACHINE" = SQL; then
    if ! test -s /etc/z-push/state-sql.conf.php; then
	if test "$POSTGRES_PASSWORD"; then
	    echo "Install Z-Push Postgres State Configuration"
	    sed -e "s|DB_DRIVER|postgres|" \
		-e "s|DB_HOST|$POSTGRES_HOST|" \
		-e "s|DB_NAME|$POSTGRES_DB|" \
		-e "s|DB_PASSWORD|$POSTGRES_PASSWORD|" \
		-e "s|DB_PORT|$POSTGRES_PORT|" \
		-e "s|DB_USER|$POSTGRES_USER|" \
		/state-sql.php >/etc/z-push/state-sql.conf.php
	elif test "$MYSQL_PASSWORD"; then
	    echo "Install Z-Push MySQL State Configuration"
	    sed -e "s|DB_DRIVER|mysql|" \
		-e "s|DB_HOST|$MYSQL_HOST|" \
		-e "s|DB_NAME|$MYSQL_DATABASE|" \
		-e "s|DB_PASSWORD|$MYSQL_PASSWORD|" \
		-e "s|DB_PORT|$MYSQL_PORT|" \
		-e "s|DB_USER|$MYSQL_USER|" \
		/state-sql.php >/etc/z-push/sql-state.conf.php
	fi
    fi
fi

if ! test -s /etc/z-push/autodiscover.conf.php; then
    echo "Install Z-Push Autodiscover Configuration"
    sed -e "s|APACHE_DOMAIN|$APACHE_DOMAIN|" \
	-e "s|DO_DEBUG|$DO_DEBUG|" \
	-e "s|PUBLIC_PROTO|$PUBLIC_PROTO|" \
	-e "s|TZ|$TZ|" \
	/autodiscover.php >/etc/z-push/autodiscover.conf.php
    cat /policies.ini >/etc/z-push/policies.ini
fi

if ! ls /etc/apache2/sites-enabled/*.conf >/dev/null 2>&1; then
    echo "Generates Z-Push VirtualHost Configuration"
    (
	cat <<EOF
<VirtualHost *:$APACHE_HTTP_PORT>
    ServerName $APACHE_DOMAIN
    CustomLog /dev/stdout modremoteip
    Include "/etc/apache2/$SSL_INCLUDE.conf"
    AddDefaultCharset UTF-8
    DirectoryIndex index.php
    DocumentRoot /usr/share/z-push
    Alias /Microsoft-Server-ActiveSync /usr/share/z-push/index.php
    Alias /AutoDiscover/AutoDiscover.xml /usr/share/z-push/autodiscover/autodiscover.php
    Alias /Autodiscover/Autodiscover.xml /usr/share/z-push/autodiscover/autodiscover.php
    Alias /autodiscover/autodiscover.xml /usr/share/z-push/autodiscover/autodiscover.php
    <Directory /usr/share/z-push>
	DirectoryIndex index.php
	Options -Indexes +FollowSymLinks
	AllowOverride none
	Require all granted
	<Files "config.php">
	    Require all denied
	</Files>
    </Directory>
</VirtualHost>
EOF
    ) >/etc/apache2/sites-enabled/003-vhosts.conf
fi

. /run-apache.sh
