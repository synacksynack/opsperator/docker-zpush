<?php
$config = array();
$config['db_dsnw'] = 'DBURL';
$config['mail_domain'] = 'APACHE_DOMAIN';
$config['smtp_helo_host'] = 'SMTP_HELO';

// https://forum.kopano.io/topic/1728/zpush-with-backend-combined/2
// https://raw.githubusercontent.com/EGroupware/z-push/master/src/config.php

define('BASE_PATH', dirname($_SERVER['SCRIPT_FILENAME']). '/');
define('CAINFO', '/etc/ssl/certs/ca-certificates.crt');
define("CERTIFICATE_OWNER_PARAMETER", "SSL_CLIENT_S_DN_CN");
define('IPC_PROVIDER', 'DO_IPC');
define('LOGAUTHFAIL', false);
define('LOGBACKEND', 'filelog');
define('LOGFILEDIR', '/var/log/z-push/');
define('LOGERRORFILE', LOGFILEDIR . 'z-push-error.log');
define('LOGFILE', LOGFILEDIR . 'z-push.log');
define('LOGLEVEL', LOGLEVEL_DO_DEBUG);
define('LOGUSERLEVEL', LOGLEVEL_DEVICEID);
define('LOG_SYSLOG_HOST', false);
define('LOOSE_PROVISIONING', false);
define('PROVISIONING', true);
define('SCRIPT_TIMEOUT', 0);
define('STATE_DIR', '/var/lib/z-push/');
define('STATE_MACHINE', 'STATEMACHINE');
define('TIMEZONE', 'TZ');
define('USE_CUSTOM_REMOTE_IP_HEADER', false);
define('USE_FULLEMAIL_FOR_LOGIN', true);

define('PROVISIONING_POLICYFILE', 'policies.ini');

define('ALLOW_WEBSERVICE_USERS_ACCESS', false);
define('FILEAS_ORDER', SYNC_FILEAS_LASTFIRST);
define('PING_INTERVAL', 30);
define('PING_LOWER_BOUND_LIFETIME', false);
define('PING_HIGHER_BOUND_LIFETIME', false);
define('RETRY_AFTER_DELAY', 300);
#define('SYNC_CONFLICT_DEFAULT', SYNC_CONFLICT_OVERWRITE_PIM);
define('SYNC_CONFLICT_DEFAULT', SYNC_CONFLICT_OVERWRITE_SERVER);
define('SYNC_CONTACTS_MAXPICTURESIZE', 5242880);
define('SYNC_FILTERTIME_MAX', SYNC_FILTERTYPE_ALL);
define('SYNC_MAX_ITEMS', 512);
define('SYNC_TIMEOUT_LONG_DEVICETYPES',   "iPod, iPad, iPhone, WP, WindowsOutlook, WindowsMail");
define('SYNC_TIMEOUT_MEDIUM_DEVICETYPES', "SAMSUNGGTI");
define('UNSET_UNDEFINED_PROPERTIES', false);
define('USE_PARTIAL_FOLDERSYNC', false);

define('BACKEND_PROVIDER', 'BackendIMAP');
define('SEARCH_PROVIDER', 'BackendSearchLDAP');
define('SEARCH_WAIT', 10);
define('SEARCH_MAXRESULTS', 10);

define('KOE_CAPABILITY_GAB', true);
define('KOE_CAPABILITY_RECEIVEFLAGS', true);
define('KOE_CAPABILITY_SENDFLAGS', true);
define('KOE_CAPABILITY_OOF', true);
define('KOE_CAPABILITY_OOFTIMES', true);
define('KOE_CAPABILITY_NOTES', true);
define('KOE_CAPABILITY_SHAREDFOLDER', true);
define('KOE_CAPABILITY_SENDAS', true);
define('KOE_CAPABILITY_SECONDARYCONTACTS', true);
define('KOE_CAPABILITY_SIGNATURES', true);
define('KOE_CAPABILITY_RECEIPTS', true);
define('KOE_CAPABILITY_IMPERSONATE', true);
define('KOE_GAB_STORE', 'SYSTEM');
define('KOE_GAB_FOLDERID', '');
define('KOE_GAB_NAME', 'Z-Push-KOE-GAB');

$additionalFolders = array(
/*
        array(
		'store'     => "SYSTEM",
		'folderid'  => "",
		'name'      => "Public Contacts",
		'type'      => SYNC_FOLDER_TYPE_USER_CONTACT,
		'flags'     => DeviceManager::FLD_FLAGS_NONE,
	    ),
*/
    );

$specialLogUsers = array();
