<?php
define('STATE_SQL_ENGINE', 'DB_DRIVER');
define('STATE_SQL_SERVER', 'DB_HOST');
define('STATE_SQL_PORT', 'DB_PORT');
define('STATE_SQL_DATABASE', 'DB_NAME');
define('STATE_SQL_USER', 'DB_USER');
define('STATE_SQL_PASSWORD', 'DB_PASSWORD');
define('STATE_SQL_OPTIONS', serialize(array(PDO::ATTR_PERSISTENT => true)));
